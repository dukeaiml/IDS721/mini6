use lambda_runtime::{run, service_fn, Error, LambdaEvent};
use std::collections::HashMap;
use lazy_static::lazy_static;
use serde::{Deserialize, Serialize};
use tracing::{info, warn, Level};
use tracing_subscriber::FmtSubscriber;

lazy_static! {
    static ref FRUITS_INFO: HashMap<String, f64> = {
        let mut map = HashMap::new();
        map.insert("apple".to_string(), 3.0);
        map.insert("orange".to_string(), 2.0);
        map.insert("pear".to_string(), 1.5);
        map
    };
}

#[derive(Deserialize)]
struct Request {
    fruitlist: Vec<String>,
}

#[derive(Serialize)]
struct Response {
    totalprice: f64
}

async fn function_handler(event: LambdaEvent<Request>) -> Result<Response, Error> {
    info!("Starting the lambda function...");

    let fruitlist = event.payload.fruitlist;
    let mut totalprice = 0.0;

    for fruit in fruitlist.iter() {
        if let Some(&value) = FRUITS_INFO.get(fruit) {
            totalprice += value;
            info!("Value of {} is: {}.", fruit, value);
        } else {
            warn!("Fruit {} doesn't exist.", fruit);
        }
    }

    let resp = Response { totalprice };

    Ok(resp)
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let subscriber = FmtSubscriber::builder()
        .with_max_level(Level::INFO)
        .finish();
    tracing::subscriber::set_global_default(subscriber)
        .expect("Failed to set default subscriber");

    run(service_fn(function_handler)).await
}
