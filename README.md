# mini6

This project extends from independent-project-4 to integrate 2 separate lambda functions into 1 and adds logging, X-Ray tracing and CloudWatch centralization to it.


## Requirements
- [x] Logging implementation to the Rust Lambda function (30%)
- [x] Integrate AWS X-Ray Tracing (30%)
- [x] Connect logs/traces to CloudWatch (30%)
- [x] Documentation (10%)

## Steps 
1. Install **Cargo Lambda**
```
brew tap cargo-lambda/cargo-lambda
brew install cargo-lambda
```

2. Create new project
```
cargo lambda new mini2 \
    && cd mini2
```

3. Add logging libraries
```
cargo add tracing
cargo add tracing-subscriber
```

4. Logging level of the tracing crate
TRACE, DEBUG, INFO, WARN, ERROR

5. Test
```
cargo lambda watch
cargo lambda invoke --data-ascii "{ \"fruitlist\": [\"apple\",\"orange\"] }" // No WARN logging
cargo lambda invoke --data-ascii "{ \"fruitlist\": [\"apple\",\"orange\",\"strawberry\"] }" // WARN logging because 'strawberry' doesn't exists
```

5. Build and Deploy
```
cargo lambda build --release
cargo lambda deploy
```

6. Enable X-Ray Tracing and CloudWatch in the Configuration tab
![Configuration](/screenshots/ConfigTab.png)

## Screenshots
1. Logg output using the tracing crate
![Logging](/screenshots/Logging.png)

2. XRay enabled
![EnabledLog](/screenshots/EnabledXRAY.png)

3. XRay Trace
![Trace](/screenshots/XRayTrace.png)

4. Cloudwatch
![Cloudwatch](/screenshots/Cloudwatch.png)
![TraceDetail](/screenshots/TraceDetail.png)
